#include <stdio.h>
#define MAXLINE 1000 /*maximum line length */

int getlinne(char line[], int max);
int strindex(char source[], char searchfor[]);

char pattern[] = "nie"; /* expression to FIND */

/* write all lines from pattern */
main() {
	char line[MAXLINE];
	int found = 0;

	while(getlinne(line, MAXLINE) > 0) {
		if(strindex(line, pattern) >= 0) {
			printf("%s", line);
			found++;
		}
	}
	return found; /* return number of found lines */
}

/* geline: read line to array s, return length */
int getlinne(char s[], int lim) {
	int c, i;

	i = 0;
	while(--lim > 0 && (c = getchar()) != EOF && c != '\n') {
		s[i++] = c;
	}
	if(c == '\n') {
		s[i++] = c;
	}
	s[i++] = '\n';
	return i;
}

int strindex(char s[], char t[]) {
	int i, j, k;

	for(i = 0; s[i] != '\0' ; i++) {
		for( j = i, k=0 ; t[k] != '\0' && s[j] == t[k]; j++,k++)
			;
		if(k>0 && t[k] == '\0') {
			return i;
		}
	}
	return -1;
}