#ifndef _ex21_h
#define _ex21_h

extern int THE_SIZE;

int get_age();
void set_age(int age);

double update_ratio(double ratio);

void print_size();

#endif