#include "dbg.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>

#define EOL '\n'

#define TILDE '~'
#define CONFIG_FILE "~/.logfind"
#define PATH_LENGTH 255

void dupa_debug() {
	printf("dupa\n");
    fflush(stdout);
}

typedef struct logfind_config {
	char *logfiles[PATH_LENGTH];
	int  size;
} logfind_config;



/* count lines of file */
int lc(FILE *file) {
	int lines = 1;
	char ch;
	while(!feof(file)) {
  		ch = fgetc(file);
  		if(ch == EOL) {
    		lines++;
  		}
  	}
	return lines;
}

/* returns user home directory */
char *GetHomeDirLocation() {
	char *homedir;
	
	if ((homedir = getenv("HOME")) == NULL) {
    	homedir = getpwuid(getuid())->pw_dir;
	}
	return homedir;
}

/* expands config file name when it starfs from '~' */
char *GetConfigFileName(char *name) {
	
	fflush(stdout);
	if(*name == TILDE) {
		char *configFileName;
		configFileName = GetHomeDirLocation();
		strcat(configFileName, &name[1]);
		return configFileName;
	} else {
		return name;
	}

}

/* reads file and returns it content as char* 
 * code from stackoverflows oblivion
 */
char* ReadFile(char *filename)
{
   char *buffer = NULL;
   int string_size,read_size;
   FILE *handler = fopen(filename,"r");

   if (handler)
   {
       fseek(handler,0,SEEK_END);
       string_size = ftell (handler);
       rewind(handler);

       buffer = (char*) malloc (sizeof(char) * (string_size + 1) );
       read_size = fread(buffer,sizeof(char),string_size,handler);
       //fread doesnt set it so put a \0 in the last position
       //and buffer is now officialy a string
       buffer[string_size] = '\0';

       if (string_size != read_size) {
           //something went wrong, throw away the memory and set
           //the buffer to NULL
           free(buffer);
           buffer = NULL;
        }
    }

    return buffer;
}

struct logfind_config *ReadConfig(char *filename) {
	

	char *configFileName = GetConfigFileName(filename);
	char *configContent = ReadFile(configFileName);

    if(NULL != configContent) {
    	printf("%s", tekst);
    }




struct logfind_config *config =  malloc(sizeof(struct logfind_config));
	int lcount = lc(file);

	config->size =  lcount;

	char *filenames = malloc(config->size * sizeof(char *));
	config->logfiles[0] = filenames;

	char *line = malloc(PATH_LENGTH);
	int insize;
	int i = 0;
	char *read;

	if(NULL != file) {

		read = fgets(line, sizeof(PATH_LENGTH), file);

		while(NULL != (read = fgets(line, sizeof(PATH_LENGTH), file))) {

			
			//printf("%s\n", line);
			config->logfiles[i] = malloc(strlen(line));
			config->logfiles[i] = line;
			//config->logfiles[i] = line;
			printf("%s\n", config->logfiles[i]);
			i++;
		}
	} else {
		printf("Error config file is null");
	}
	free(line);

	free(configContent);
	return config;
}
int main(int argc, char *argv[]) {

    
	ReadConfig(CONFIG_FILE);
    
   
    return 0;
//error:
//	return -1;
}