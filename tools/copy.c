/*
 * Program copy files from standard input to standard output
  for tests only - non production version
 */

#include <stdio.h>

#define ENDLINE '\0'

void copy() {
	char c;

	do {
		c = fgetc(stdin);
		fputc(c, stdout);
	} while(c != EOF);
	putchar(ENDLINE);
	
}

int main(int argc, char *argv[]) {
	copy();
 	return 0;
}



